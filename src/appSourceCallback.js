define("sample.webapp.racer.pkgapp.AppSourceCallback", ["require", "exports", "bluebird", "platform.shell.PlatformShellExtension", "sample.webapp.racer.RacerExtension"], function (require, exports, Promise, platform_shell_PlatformShellExtension_1, sample_webapp_racer_RacerExtension_1) {
    "use strict";
    var _this = this;
    var makeNewRuntime = function (repo) {
        var appRuntime = platform_shell_PlatformShellExtension_1.PlatformShellExtension.get(repo.runtime).makeAppRuntime({
            "sample.webapp.racer": {
                repoId: repo.config.id,
            },
        }, [
            new sample_webapp_racer_RacerExtension_1.RacerExtension(),
        ]);
        appRuntime.hookLogMessage.on(_this, function (log) {
            repo.runtime.hookLogMessage.emit(log);
        });
        return appRuntime;
    };
    var appSourceCallback = {
        cleared: function (repo) {
            return Promise.resolve();
        },
        fetched: function (repo) {
            return Promise.resolve();
        },
        run: function (repo) {
            var runPromise = Promise.resolve(true);
            if (repo.sourceHash.last() !== repo.sourceHash.last(1)) {
                var oldRuntime_1 = repo.getData("sample.webapp.racer.pkgapp.AppSourceCallback.Runtime");
                runPromise = runPromise.then(function () {
                    var currentRuntime = makeNewRuntime(repo);
                    repo.setData("sample.webapp.racer.pkgapp.AppSourceCallback.Runtime", currentRuntime);
                    return currentRuntime.load();
                });
                if (oldRuntime_1 !== undefined) {
                    runPromise = runPromise.then(function () {
                        return oldRuntime_1.unload();
                    });
                }
            }
            return runPromise;
        },
    };
    return appSourceCallback;
});
